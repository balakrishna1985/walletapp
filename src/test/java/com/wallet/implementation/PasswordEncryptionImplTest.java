package com.wallet.implementation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class PasswordEncryptionImplTest {

    @InjectMocks
    private PasswordEncryptionImpl passwordEncryptionImpl;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @Test
    void encrypt(){
        String encyptedPassword=passwordEncryptionImpl.encrypt("admin");
        assertNotNull(encyptedPassword);
    }

    @Test
    void decrypt(){
        String decryptedPassword=passwordEncryptionImpl.encrypt("YWRtaW4=");
        assertNotEquals("admin",decryptedPassword);
    }
}