package com.wallet.implementation;

import com.wallet.entity.Balances;
import com.wallet.entity.Customer;
import com.wallet.repository.BalanceRepository;
import com.wallet.repository.CustomerRepository;
import com.wallet.vo.LoginVo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import javax.security.auth.login.LoginException;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;

@ExtendWith(SpringExtension.class)
class CustomerServiceImplTest {

    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private BalanceRepository balanceRepository;

    @Mock
    private PasswordEncryptionImpl passwordEncryptionImpl;

    @InjectMocks
    private CustomerServiceImpl customerServiceImpl;

    @MockBean
    LoginVo loginVo;
    @MockBean
    Balances balances;
    @MockBean
    Customer customer;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @Test
    void getCustomerByCustId(){

        Customer mockCustomer=Customer.builder().cifNumber("12345").firstName("Alice").lastName("").build();
        Mockito.when(customerRepository.getByCustomerId(Mockito.anyString())).thenReturn(mockCustomer);
        LoginVo loginVo=customerServiceImpl.getCustomerByCustId("12345");
        assertEquals(loginVo.getCustomerId(),mockCustomer.getCustomerId());
    }
    @Test
    void saveCustomer() throws Exception {
        Customer mockCustomer=Customer.builder().customerId("alice1985").password("password")
                .cifNumber("12345").firstName("Alice")
                .lastName("").createdBy("Alice").updatedBy("Alice").build();
        Balances mockBalance=Balances.builder().cifNumber(mockCustomer.getCifNumber()).balance(new BigDecimal("0.00")).build();
        Mockito.when(passwordEncryptionImpl.encrypt(any())).thenReturn("234323399399993");

        Mockito.when(customerRepository.save(any())).thenReturn(mockCustomer);
        Mockito.when(balanceRepository.save(any())).thenReturn(mockBalance);

        customerServiceImpl.saveCustomer(mockCustomer);
        assertEquals("0.00",mockBalance.getBalance());
    }

    @Test
    void doLogin() throws Exception {
        Customer mockCustomer=Customer.builder().customerId("alice1985").password("password")
                .cifNumber("12345").firstName("Alice").build();
        LoginVo mockLoginVo=LoginVo.builder().customerId("Alice1985").password("admin").build();
        Mockito.when(passwordEncryptionImpl.encrypt(any())).thenReturn("234323399399993");
        Mockito.when(customerRepository.doLogin(Mockito.anyString(),Mockito.anyString())).thenReturn(mockCustomer);
        LoginVo loginVo= customerServiceImpl.doLogin(mockLoginVo);
    }

    @Test
    void doLogin_failed() throws Exception {
        Customer mockCustomer=Customer.builder().customerId("alice1985").password("password")
                .cifNumber("12345").firstName("Alice").build();
        LoginVo mockLoginVo=LoginVo.builder().customerId("Alice1985").password("admin").build();
        Mockito.when(passwordEncryptionImpl.encrypt(any())).thenReturn("234323399399993");
        Mockito.when(customerRepository.doLogin(Mockito.anyString(),Mockito.anyString())).thenReturn(null);
        try{
            LoginVo loginVo = customerServiceImpl.doLogin(mockLoginVo);
        }catch (LoginException ex){
            ex.getMessage();
        }

    }

    @Test
    void doLogin_Threshold_failed() throws Exception {
        Customer mockCustomer=Customer.builder().customerId("alice1985").password("password")
                .cifNumber("12345").firstName("Alice").loginFailedCount(6).build();
        LoginVo mockLoginVo=LoginVo.builder().customerId("Alice1985").password("admin").build();
        Mockito.when(passwordEncryptionImpl.encrypt(any())).thenReturn("234323399399993");
        Mockito.when(customerRepository.doLogin(Mockito.anyString(),Mockito.anyString())).thenReturn(mockCustomer);
        try {
            LoginVo loginVo = customerServiceImpl.doLogin(mockLoginVo);
        }catch (LoginException ex){
            ex.getMessage();
        }


    }
}