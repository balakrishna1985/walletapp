package com.wallet.implementation;

import com.wallet.entity.BalanceRelationships;
import com.wallet.entity.Balances;
import com.wallet.repository.BalanceRelationShipsRepository;
import com.wallet.repository.PaymentRepository;
import com.wallet.vo.BalanceDetailsVO;
import com.wallet.vo.BalanceVO;
import com.wallet.vo.PaymentVo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
public class BalanceServiceImplTest {

    @Mock
    private PaymentRepository paymentRepository;
    @Mock
    private BalanceRelationShipsRepository balanceRelationShipsRepository;

    @InjectMocks
    private BalanceServiceImpl BalanceServiceImpl;
    @MockBean
    PaymentVo paymentVo;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @Test
    public void getBalanceByCifNumber() throws Exception {
        paymentVo = PaymentVo.builder().cifNumber("12345").amount(new BigDecimal("100")).customerName("Alice").build();
        List<BalanceRelationships> list = Collections.emptyList();
        List<Balances> balancesList = Collections.emptyList();

        Balances mockBalances = Balances.builder().cifNumber("12345").balance(new BigDecimal("10.0")).build();
        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes(Mockito.anyString())).thenReturn(list);
        Mockito.when(paymentRepository.getBalancesByCif(Mockito.anyString())).thenReturn(mockBalances);

        Mockito.when(balanceRelationShipsRepository.retrievePayableBalanceListByCif(Mockito.anyString())).thenReturn(getMockBalaanceRelastionships());
        Mockito.when(balanceRelationShipsRepository.retrievePayeeBalanceListByCifList(Mockito.anyString())).thenReturn(getMockBalaanceRelastionships());

        BalanceVO balanceVO=BalanceServiceImpl.getBalanceByCifNumber("12345");

        assertEquals(balanceVO.getAccountBalance(),mockBalances.getBalance());
        assertNotNull(balanceVO);
    }
    public List<Object> getMockBalaanceRelastionships(){
        List<Object> payableBalanceTransList=new ArrayList<>();
        Object[] obj = new Object[] { "a", "b", "c" };
        payableBalanceTransList.add(obj);
        return payableBalanceTransList;
    }
}
