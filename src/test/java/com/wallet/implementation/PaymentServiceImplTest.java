package com.wallet.implementation;

import com.wallet.entity.BalanceRelationships;
import com.wallet.entity.Balances;
import com.wallet.entity.Customer;
import com.wallet.repository.BalanceRelationShipsRepository;
import com.wallet.repository.CustomerRepository;
import com.wallet.repository.PaymentRepository;
import com.wallet.vo.PaymentVo;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ExtendWith(SpringExtension.class)
class PaymentServiceImplTest {

    @Mock
    private PaymentRepository paymentRepository;
    @Mock
    private BalanceRelationShipsRepository balanceRelationShipsRepository;
    @Mock
    private CustomerRepository customerRepository;
    @InjectMocks
    private PaymentServiceImpl paymentServiceImpl;
    @MockBean
    PaymentVo paymentVo;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @Test
    void testTopupAlice() throws Exception {
        paymentVo = PaymentVo.builder().cifNumber("12345").amount(new BigDecimal("100")).customerName("Alice").build();
        List<BalanceRelationships> list = Collections.emptyList();
        List<Balances> balancesList = Collections.emptyList();
        Balances mockBalances = Balances.builder().cifNumber("12345").balance(new BigDecimal("0.00")).build();
        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes(Mockito.anyString())).thenReturn(list);
        Mockito.when(paymentRepository.getBalancesByCif(Mockito.anyString())).thenReturn(mockBalances);
        paymentVo = paymentServiceImpl.topUp(paymentVo);
        Assert.assertEquals(paymentVo.getAmount(), new BigDecimal("100"));
    }

    @Test
    void testTransfer_inSuffientBalance() throws Exception {
        paymentVo = PaymentVo.builder().fromCustomerId("12345").toCustomerId("67890").amount(new BigDecimal("100")).customerName("Alice").build();
        Customer fromCustomer=Customer.builder().cifNumber("12345").build();
        Customer toCustomer=Customer.builder().cifNumber("12345").build();
        List<BalanceRelationships> list = Collections.emptyList();
        Balances mockBalances = Balances.builder().cifNumber("12345").balance(new BigDecimal("0.00")).build();
        Mockito.when(customerRepository.getByCustomerId("12345")).thenReturn(fromCustomer);
        Mockito.when(customerRepository.getByCustomerId("67890")).thenReturn(toCustomer);
        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes(Mockito.anyString())).thenReturn(list);
        Mockito.when(paymentRepository.getBalancesByCif(Mockito.anyString())).thenReturn(mockBalances);
        paymentVo = paymentServiceImpl.transfer(paymentVo);
        Assert.assertEquals(paymentVo.getAmount(), new BigDecimal("100"));
    }
    @Test
    void testTransfer_suffientBalance() throws Exception {
        paymentVo = PaymentVo.builder().fromCustomerId("12345").toCustomerId("67890")
                .amount(new BigDecimal("100")).customerName("Alice").build();
        List<BalanceRelationships> list = Collections.emptyList();
        Customer fromCustomer=Customer.builder().cifNumber("12345").build();
        Customer toCustomer=Customer.builder().cifNumber("67890").build();
        Balances mockFromBalances = Balances.builder().cifNumber("12345").balance(new BigDecimal("200.0")).build();

        Balances toBalances = Balances.builder().cifNumber("56789").balance(new BigDecimal("200.0")).build();
        Mockito.when(customerRepository.getByCustomerId("12345")).thenReturn(fromCustomer);
        Mockito.when(customerRepository.getByCustomerId("67890")).thenReturn(toCustomer);
        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes(Mockito.anyString())).thenReturn(list);

        Mockito.when(paymentRepository.getBalancesByCif("12345")).thenReturn(mockFromBalances);
        Mockito.when(paymentRepository.getBalancesByCif("67890")).thenReturn(toBalances);



        paymentVo = paymentServiceImpl.transfer(paymentVo);
        Assert.assertEquals(paymentVo.getAmount(), new BigDecimal("100"));
    }

    @Test
    void testTransfer_payableTransactions() throws Exception {
        paymentVo = PaymentVo.builder().fromCustomerId("12345").toCustomerId("67890")
                .amount(new BigDecimal("100")).customerName("Alice").build();
        Customer fromCustomer=Customer.builder().cifNumber("12345").build();
        Customer toCustomer=Customer.builder().cifNumber("67890").build();
        List<BalanceRelationships> balanceRelationshipsList = new ArrayList<>();

        BalanceRelationships balanceRelationships=BalanceRelationships.builder().sourceCifNumber("12345")
                .destinationCifNumber("67890").oweingBalance(new BigDecimal("100.00")).build();
        balanceRelationshipsList.add(balanceRelationships);

        Balances mockFromBalances = Balances.builder().cifNumber("12345").balance(new BigDecimal("0.00")).build();

        Balances toBalances = Balances.builder().cifNumber("56789").balance(new BigDecimal("200.0")).build();
        Mockito.when(customerRepository.getByCustomerId("12345")).thenReturn(fromCustomer);
        Mockito.when(customerRepository.getByCustomerId("67890")).thenReturn(toCustomer);
        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes(Mockito.anyString())).thenReturn(balanceRelationshipsList);

        Mockito.when(balanceRelationShipsRepository.retrieveOweBalanceByCif("12345","67890")).thenReturn(balanceRelationships);


        Mockito.when(paymentRepository.getBalancesByCif("12345")).thenReturn(mockFromBalances);
        Mockito.when(paymentRepository.getBalancesByCif("67890")).thenReturn(toBalances);

        paymentVo = paymentServiceImpl.transfer(paymentVo);
        Assert.assertEquals(paymentVo.getTotalBalance(), new BigDecimal("0.00"));
    }




    /******************************** Test cases for Assignment ******************/
    /** login as alice and top up Alice 100 , current balance is 0.00 **/
    @Test
    public void topUpAllice100_TestCase1() throws Exception {

        PaymentVo paymentVo = PaymentVo.builder().cifNumber("12345").amount(new BigDecimal("100.00")).customerName("Alice").build();
        List<BalanceRelationships> list = Collections.emptyList();
        List<Balances> balancesList = Collections.emptyList();

        Balances aliceCurrentBalanceMock = Balances.builder().cifNumber("12345").balance(new BigDecimal("0.00")).build();

        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes(Mockito.anyString())).thenReturn(list);
        Mockito.when(paymentRepository.getBalancesByCif(Mockito.anyString())).thenReturn(aliceCurrentBalanceMock);
        PaymentVo afterTopUpAlice_Account_Balance = paymentServiceImpl.topUp(paymentVo);


        Assert.assertEquals(afterTopUpAlice_Account_Balance.getTotalBalance(), new BigDecimal("100.00"));
    }

    /** login as bob and top up bob 80 , current balance is 0.00 **/
    @Test
    public void topUpBob80_TestCase2() throws Exception {

        PaymentVo paymentVo = PaymentVo.builder().cifNumber("67890").amount(new BigDecimal("80.00")).customerName("Bob").build();
        List<BalanceRelationships> list = Collections.emptyList();
        List<Balances> balancesList = Collections.emptyList();

        Balances aliceCurrentBalanceMock = Balances.builder().cifNumber("67890").balance(new BigDecimal("0.00")).build();

        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes(Mockito.anyString())).thenReturn(list);
        Mockito.when(paymentRepository.getBalancesByCif(Mockito.anyString())).thenReturn(aliceCurrentBalanceMock);
        PaymentVo afterTopUpBob_Account_Balance = paymentServiceImpl.topUp(paymentVo);


        Assert.assertEquals(afterTopUpBob_Account_Balance.getTotalBalance(), new BigDecimal("80.00"));
    }

    /** login as bob and transfer 50 to Alice, current balance Bob balance is 80.00
     *  current alice balance is 100
     *  assumption - there is no pending owes or receivable amount each other
     *  and alice also don't have any other payable amount to any other
     * **/
    @Test
    public void transfer50_to_alice_testCase3() throws Exception {

        PaymentVo bobPaymentVO = PaymentVo.builder().fromCustomerId("67890").toCustomerId("12345")
                .amount(new BigDecimal("50")).customerName("Bob").build();
        Customer fromCustomer=Customer.builder().cifNumber("67890").build();
        Customer toCustomer=Customer.builder().cifNumber("12345").build();
        /** Alice balance relationship transaction also empty **/
        List<BalanceRelationships> bobAndAliceBalanceRelationshipsList = Collections.emptyList();
        Balances mockBobCurrentBalance = Balances.builder().cifNumber("67890").balance(new BigDecimal("80.00")).build();
        Balances mockAliceCurrentBalance = Balances.builder().cifNumber("12345").balance(new BigDecimal("100.0")).build();
        Mockito.when(customerRepository.getByCustomerId("67890")).thenReturn(fromCustomer);
        Mockito.when(customerRepository.getByCustomerId("12345")).thenReturn(toCustomer);
        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes(Mockito.anyString())).thenReturn(bobAndAliceBalanceRelationshipsList);
        Mockito.when(balanceRelationShipsRepository.retrieveOweBalanceByCif("67890","12345")).thenReturn(null);
        Mockito.when(paymentRepository.getBalancesByCif("67890")).thenReturn(mockBobCurrentBalance);
        Mockito.when(paymentRepository.getBalancesByCif("12345")).thenReturn(mockAliceCurrentBalance);

        PaymentVo afterTransactionBobPaymentVo = paymentServiceImpl.transfer(bobPaymentVO);
        /** after transfer 50 dollers to Allice from bob
         * Bob balance is 30.00
         */
        Assert.assertEquals(afterTransactionBobPaymentVo.getTotalBalance(), new BigDecimal("30.00"));
    }

    /** login as bob and transfer 100 to Alice, current balance Bob balance is 30.00
     *  current alice balance is 150
     *  assumption - there is no pending owes or receivable amount each other
     *  and alice also don't have any other payable amount to any other
     * **/
    @Test
    public void transfer100_to_alice_testCase4() throws Exception {

        PaymentVo bobPaymentVO = PaymentVo.builder().fromCustomerId("67890").toCustomerId("12345")
                .amount(new BigDecimal("100")).customerName("Bob").build();
        Customer fromCustomer=Customer.builder().cifNumber("67890").build();
        Customer toCustomer=Customer.builder().cifNumber("12345").build();
        /** Alice balance relationship transaction also empty **/
        List<BalanceRelationships> bobAndAliceBalanceRelationshipsList = Collections.emptyList();
        Balances mockBobCurrentBalance = Balances.builder().cifNumber("67890").balance(new BigDecimal("30.00")).build();
        Balances mockAliceCurrentBalance = Balances.builder().cifNumber("12345").balance(new BigDecimal("150.0")).build();
        Mockito.when(customerRepository.getByCustomerId("67890")).thenReturn(fromCustomer);
        Mockito.when(customerRepository.getByCustomerId("12345")).thenReturn(toCustomer);
        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes(Mockito.anyString())).thenReturn(bobAndAliceBalanceRelationshipsList);
        Mockito.when(balanceRelationShipsRepository.retrieveOweBalanceByCif("67890","12345")).thenReturn(null);
        Mockito.when(paymentRepository.getBalancesByCif("67890")).thenReturn(mockBobCurrentBalance);
        Mockito.when(paymentRepository.getBalancesByCif("12345")).thenReturn(mockAliceCurrentBalance);

        PaymentVo afterTransactionBobPaymentVo = paymentServiceImpl.transfer(bobPaymentVO);
        /** after transfer 100 dollers to Allice from bob
         * Bob balance is 30.00
         * Alice balance is 180
         * one payable transaction from bob to alice - 70.
         */
        Assert.assertEquals(afterTransactionBobPaymentVo.getTotalBalance(), new BigDecimal("0.00"));
    }
    /** login as bob and topup 30 , current Bob balance is 0.00
     *  current alice balance is 180
     *  assumption - there is one pending owes  to alice from bob is 70
     *  and alice also don't have any other payable amount to any other
     * **/
    @Test
    public void topUpBob30_TestCase5() throws Exception {

        PaymentVo bobTopupPaymentVo = PaymentVo.builder().cifNumber("67890").amount(new BigDecimal("30.00")).customerName("Bob").build();
        List<BalanceRelationships> bobToAliceOweTransList = new ArrayList<>();

         /** there is one payable transaction relation from bot to alice and  oweiamount is also 70.00 **/
       BalanceRelationships bobAndAliceBalanceRelationshipsVO=BalanceRelationships.builder().sourceCifNumber("67890")
                .destinationCifNumber("12345").oweingBalance(new BigDecimal("70.00")).build();
        bobToAliceOweTransList.add(bobAndAliceBalanceRelationshipsVO);
        Balances aliceCurrentBalanceMock = Balances.builder().cifNumber("12345").balance(new BigDecimal("180.00")).build();
        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes("67890")).thenReturn(bobToAliceOweTransList);
        Mockito.when(paymentRepository.getBalancesByCif("12345")).thenReturn(aliceCurrentBalanceMock);
        PaymentVo afterTopUpBob_Account_Balance = paymentServiceImpl.topUp(bobTopupPaymentVo);
        /** after bob topup 30
         * Bob balance is 0.00
         * Alice balance is 210
         * one payable transaction from bob to alice - 40.
         */
        Assert.assertEquals(afterTopUpBob_Account_Balance.getTotalBalance(), new BigDecimal("0.00"));
    }

    /** login as Alice and transfer 30 to bob, current balance Bob balance is 0.00
     *  current alice balance is 210
     *  assumption - there is one pending transaction from bob and receivable amount is 40
     *  and alice also don't have any other payable amount to any other
     * **/
    @Test
    public void transfer30_to_bob_testCase6() throws Exception {

        PaymentVo alicePaymentVO = PaymentVo.builder().fromCustomerId("12345").toCustomerId("67890")
                .amount(new BigDecimal("30.00")).customerName("Alice").build();
        Customer fromCustomer=Customer.builder().cifNumber("12345").build();
        Customer toCustomer=Customer.builder().cifNumber("67890").build();
        /** Alice balance relationship transaction also empty **/
        List<BalanceRelationships> aliceAndBobBalanceRelationshipsList = new ArrayList<>();
        /** there is one payable transaction relation from bot to alice and  oweiamount is also 70.00 **/
        BalanceRelationships bobAndAliceBalanceRelationshipsVO=BalanceRelationships.builder().sourceCifNumber("67890")
                .destinationCifNumber("12345").oweingBalance(new BigDecimal("40.00")).build();
        aliceAndBobBalanceRelationshipsList.add(bobAndAliceBalanceRelationshipsVO);

        Balances mockBobCurrentBalance = Balances.builder().cifNumber("67890").balance(new BigDecimal("0.00")).build();
        Balances mockAliceCurrentBalance = Balances.builder().cifNumber("12345").balance(new BigDecimal("210.0")).build();
        Mockito.when(customerRepository.getByCustomerId("12345")).thenReturn(fromCustomer);
        Mockito.when(customerRepository.getByCustomerId("67890")).thenReturn(toCustomer);
        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes("67890")).thenReturn(aliceAndBobBalanceRelationshipsList);
        Mockito.when(balanceRelationShipsRepository.retrieveOweBalanceByCif("12345","67890")).thenReturn(null);

        Mockito.when(paymentRepository.getBalancesByCif("67890")).thenReturn(mockBobCurrentBalance);
        Mockito.when(paymentRepository.getBalancesByCif("12345")).thenReturn(mockAliceCurrentBalance);

        PaymentVo afterTransactionAlicePaymentVo = paymentServiceImpl.transfer(alicePaymentVO);
        /** after transfer 30 dollers to bob from alice
         * Bob balance is 0.00
         * Alice balance is 210.00
         * one payable transaction from bob to alice - 10.00.
         */
        Assert.assertEquals(afterTransactionAlicePaymentVo.getTotalBalance(), new BigDecimal("210.00"));
    }
    @Test
    public void topUpBob100_TestCase7() throws Exception {

        PaymentVo bobTopupPaymentVo = PaymentVo.builder().cifNumber("67890").amount(new BigDecimal("100.00")).customerName("Bob").build();
        List<BalanceRelationships> bobToAliceOweTransList = new ArrayList<>();

        /** there is one payable transaction relation from bot to alice and  oweiamount is also 10.00 **/
        BalanceRelationships bobAndAliceBalanceRelationshipsVO=BalanceRelationships.builder().sourceCifNumber("67890")
                .destinationCifNumber("12345").oweingBalance(new BigDecimal("10.00")).build();
        bobToAliceOweTransList.add(bobAndAliceBalanceRelationshipsVO);

        Balances bobCurrentBalanceMock = Balances.builder().cifNumber("67890").balance(new BigDecimal("0.00")).build();
        Mockito.when(balanceRelationShipsRepository.retrieveDestinationOwes("67890")).thenReturn(bobToAliceOweTransList);
        Mockito.when(paymentRepository.getBalancesByCif("67890")).thenReturn(bobCurrentBalanceMock);
        PaymentVo afterTopUpBob_Account_Balance = paymentServiceImpl.topUp(bobTopupPaymentVo);
        /** after bob topup 100
         * Bob balance is 90.00
         * Alice balance is 220
         * one payable transaction from bob to alice - 0.00.
         */
        Assert.assertEquals(afterTopUpBob_Account_Balance.getTotalBalance(), new BigDecimal("90.00"));
    }

}