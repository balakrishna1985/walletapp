package com.wallet.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wallet.service.CustomerService;
import com.wallet.service.PaymentService;
import com.wallet.vo.LoginVo;
import com.wallet.vo.PaymentVo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PaymentControllerTest {

    @MockBean
    private PaymentService paymentService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void balanceTopup() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        PaymentVo paymentVo = PaymentVo.builder().fromCustomerId("12345").amount(new BigDecimal("10.00")).build();
        String json = mapper.writeValueAsString(paymentVo);
        Mockito.when(paymentService.topUp(any())).thenReturn(paymentVo);
        mockMvc.perform(post("/api/balance/topup").contentType(MediaType.APPLICATION_JSON).content(json).characterEncoding("utf-8").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void balanceTransfer() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        PaymentVo paymentVo = PaymentVo.builder().fromCustomerId("12345").toCustomerId("67890").amount(new BigDecimal("10.00")).build();
        String json = mapper.writeValueAsString(paymentVo);
        Mockito.when(paymentService.topUp(any())).thenReturn(paymentVo);
        mockMvc.perform(post("/api/balance/transfer").contentType(MediaType.APPLICATION_JSON).content(json).characterEncoding("utf-8").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}