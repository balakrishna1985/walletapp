package com.wallet.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wallet.service.CustomerService;
import com.wallet.vo.LoginVo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class LoginControllerTest {

    @MockBean
    private CustomerService customerService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void doLogin() throws Exception {
        LoginVo loginVO = LoginVo.builder().customerId("Alice1985").password("admin").build();
        Mockito.when(customerService.doLogin(any())).thenReturn(loginVO);

        ObjectMapper mapper = new ObjectMapper();
        LoginVo loginVo = LoginVo.builder().customerId("1234").password("1234").status("success").build();
        String json = mapper.writeValueAsString(loginVo);
        Mockito.when(customerService.saveCustomer(any())).thenReturn(loginVo);
        mockMvc.perform(post("/api/doLogin").contentType(MediaType.APPLICATION_JSON).content(json).characterEncoding("utf-8").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}