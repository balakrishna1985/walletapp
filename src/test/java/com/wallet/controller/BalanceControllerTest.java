package com.wallet.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wallet.service.BalanceService;
import com.wallet.vo.BalanceVO;
import com.wallet.vo.LoginVo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class BalanceControllerTest {

    @MockBean
    private BalanceService balanceService;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void balanceSummaryTest() throws Exception {
        BalanceVO balanceVO = BalanceVO.builder().accountBalance(new BigDecimal("30.00")).build();
        Mockito.when(balanceService.getBalanceByCifNumber(any())).thenReturn(balanceVO);
        balanceService.getBalanceByCifNumber("12345");
        ObjectMapper mapper = new ObjectMapper();
        LoginVo loginVo = LoginVo.builder().customerId("1234").password("1234").status("success").build();
        String json = mapper.writeValueAsString(loginVo);
        mockMvc.perform(post("/api/balance/summary/{cifNumber}","12345").contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

}
