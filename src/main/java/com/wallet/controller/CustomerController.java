package com.wallet.controller;

import com.wallet.entity.Customer;
import com.wallet.service.CustomerService;
import com.wallet.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.security.auth.login.LoginException;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;
    /**
     *
     * @param customerId
     * @return customer details
     *
     */
    @PostMapping ("/api/customer/get/{customerId}")
    public LoginVo getCustomer(@PathVariable(name = "customerId") String customerId) {
        return customerService.getCustomerByCustId(customerId);
    }

    /**
     *
     * @param customer
     */
    @PostMapping("/api/customer/register")
    public LoginVo saveCustomer(@RequestBody Customer customer) throws LoginException {
        LoginVo loginVo= customerService.saveCustomer(customer);
        System.out.println("Customer Saved Successfully");
        return loginVo;
    }

}
