package com.wallet.controller;

import com.wallet.service.BalanceService;
import com.wallet.vo.BalanceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BalanceController {

    @Autowired
    private BalanceService balanceService;

    /**
     *
     * @param cifNumber
     * @return
     * @throws Exception
     */
    @PostMapping("/api/balance/summary/{cifNumber}")
    public BalanceVO balanceSummary(@PathVariable(name = "cifNumber") String cifNumber ) throws Exception {
        return balanceService.getBalanceByCifNumber(cifNumber);
    }
}
