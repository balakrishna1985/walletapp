package com.wallet.controller;

import com.wallet.service.PaymentService;
import com.wallet.vo.PaymentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    /**
     *
     * @param paymentVo
     * @return
     * @throws Exception
     */
    @PostMapping("/api/balance/topup")
    public PaymentVo balanceTopup(@RequestBody PaymentVo paymentVo) throws Exception {
        return paymentService.topUp(paymentVo);
    }

    /**
     *
     * @param paymentVo
     * @return
     * @throws Exception
     */
    @PostMapping("/api/balance/transfer")
    public PaymentVo balanceTransfer(@RequestBody PaymentVo paymentVo) throws Exception {
        return paymentService.transfer(paymentVo);
    }
}
