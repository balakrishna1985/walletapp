package com.wallet.controller;

import com.wallet.service.CustomerService;
import com.wallet.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.security.auth.login.LoginException;

@RestController
@Validated
public class LoginController {

    @Autowired
    private CustomerService customerService;

    /**
     *
     * @param loginVo
     * @return
     * @throws LoginException
     */
    @PostMapping("/api/doLogin")
    public LoginVo doLogin(@RequestBody LoginVo loginVo) throws LoginException {
        return customerService.doLogin(loginVo);
    }
}
