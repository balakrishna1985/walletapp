package com.wallet.repository;

import com.wallet.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {

    @Query("SELECT customer FROM Customer customer WHERE customer.customerId = ?1 and customer.password = ?2")
    public Customer doLogin(String customerId,String password);

    @Query("SELECT customer FROM Customer customer WHERE customer.customerId = ?1")
    public Customer getByCustomerId(String customerId);

    @Query(value = "SELECT CIF_SEQ.nextval FROM DUAL", nativeQuery = true)
    Long getNextCifNoSeq();
}
