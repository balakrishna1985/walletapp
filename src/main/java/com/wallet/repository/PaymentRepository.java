package com.wallet.repository;

import com.wallet.entity.Balances;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface PaymentRepository extends JpaRepository<Balances,Long> {

    @Query("SELECT balances FROM Balances balances WHERE balances.cifNumber = ?1")
    public Balances getBalancesByCif(String cif);

    @Query("update Balances balances set balances.balance=?1 WHERE balances.cifNumber = ?2")
    public Balances updateBalancesByCif(BigDecimal amount , String cif);
}
