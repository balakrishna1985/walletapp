package com.wallet.repository;

import com.wallet.entity.BalanceRelationships;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BalanceRelationShipsRepository extends JpaRepository<BalanceRelationships, Long> {

    /* to retrieve owing list that I need to pay with ascending order. */
    @Query("SELECT balanceRelationships FROM BalanceRelationships balanceRelationships WHERE balanceRelationships.oweingBalance>0 and balanceRelationships.sourceCifNumber = ?1 order by balanceRelationships.createdDate ")
    public List<BalanceRelationships> retrieveDestinationOwes(String cif);

    @Query("SELECT balanceRelationships FROM BalanceRelationships balanceRelationships WHERE balanceRelationships.sourceCifNumber = ?1  and balanceRelationships.destinationCifNumber = ?2 ")
    public BalanceRelationships retrieveOweBalanceByCif(String fromCif, String toCif);


    @Query("SELECT cus.firstName,cus.lastName, bal.oweingBalance FROM BalanceRelationships bal ,Customer cus WHERE bal.sourceCifNumber = ?1  and bal.sourceCifNumber=cus.cifNumber and bal.oweingBalance>0 order by bal.createdDate desc ")
    public List<Object> retrievePayableBalanceListByCif(String fromCif);

    @Query("SELECT cus.firstName as firstName,cus.lastName as lastName, bal.oweingBalance as balance FROM BalanceRelationships bal ,Customer cus WHERE bal.destinationCifNumber = ?1  and bal.sourceCifNumber=cus.cifNumber and bal.oweingBalance>0 order by bal.createdDate desc ")
    public List<Object> retrievePayeeBalanceListByCifList(String fromCif);




}
