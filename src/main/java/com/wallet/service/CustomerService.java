package com.wallet.service;

import com.wallet.entity.Customer;
import com.wallet.vo.LoginVo;

import javax.security.auth.login.LoginException;

public interface CustomerService {

     LoginVo getCustomerByCustId(String customerId);

     LoginVo saveCustomer(Customer customer) throws LoginException;

     LoginVo doLogin(LoginVo loginVo) throws LoginException;


}
