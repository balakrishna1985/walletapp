package com.wallet.service;

public interface PasswordEncryptionService {
    public String encrypt(String password);
    public String decrypt(String encryptPassword);
}
