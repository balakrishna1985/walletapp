package com.wallet.service;

import com.wallet.vo.BalanceVO;

public interface BalanceService {
    BalanceVO getBalanceByCifNumber(String cifNumber) throws Exception;
}
