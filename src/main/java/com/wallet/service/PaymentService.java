package com.wallet.service;

import com.wallet.entity.Customer;
import com.wallet.vo.PaymentVo;

public interface PaymentService {
    PaymentVo topUp(PaymentVo paymentVo) throws Exception;
    PaymentVo transfer(PaymentVo paymentVo) throws Exception;
    Customer getCustomerDetailsByCif(String customer);
}
