package com.wallet.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.math.BigDecimal;

@Setter
@Getter
@AllArgsConstructor
@Builder(toBuilder=true)
@Data
@NoArgsConstructor
public class PaymentVo {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal amount;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String cifNumber;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String fromCustomerId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal totalBalance;

    //used only for transfer
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String toCustomerId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String toCifNumber;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String customerName;
}
