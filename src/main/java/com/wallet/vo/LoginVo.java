package com.wallet.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@Builder(toBuilder=true)
@NoArgsConstructor
@Data
public class LoginVo {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String customerId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String cifNumber;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String name;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String statusCode;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String status;

}
