package com.wallet.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@Builder(toBuilder=true)
@NoArgsConstructor
@Data
public class BalanceVO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal accountBalance;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<BalanceDetailsVO> payableBalanceTransList;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<BalanceDetailsVO> payeeBalanceTransList;
}
