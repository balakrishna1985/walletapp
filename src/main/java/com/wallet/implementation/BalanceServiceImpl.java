package com.wallet.implementation;

import com.wallet.entity.Balances;
import com.wallet.exceptions.PaymentException;
import com.wallet.repository.BalanceRelationShipsRepository;
import com.wallet.repository.PaymentRepository;
import com.wallet.service.BalanceService;
import com.wallet.vo.BalanceDetailsVO;
import com.wallet.vo.BalanceVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class BalanceServiceImpl implements BalanceService {

    private static final Logger logger = LoggerFactory.getLogger(BalanceServiceImpl.class);

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private BalanceRelationShipsRepository balanceRelationShipsRepository;


    @Override
    @Transactional
    public BalanceVO getBalanceByCifNumber(String cifNumber) throws Exception {
        logger.info("Start account summary dao call");
        BalanceVO balanceVO =null;
        try {
            Balances existingCustBalance = paymentRepository.getBalancesByCif(cifNumber);
            balanceVO=BalanceVO.builder().accountBalance(existingCustBalance.getBalance())
                    .payableBalanceTransList(getPayableTranscationList(cifNumber))
                    .payeeBalanceTransList(getPayeeTranscationList(cifNumber)).build();
            logger.info("End account summary dao call");
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new PaymentException("Cif number not found in the system" + cifNumber);
        }
        return balanceVO;
    }

    private List<BalanceDetailsVO> getPayableTranscationList(String cifNumber){
        List<Object> payableBalanceTransList = balanceRelationShipsRepository.retrievePayableBalanceListByCif(cifNumber);
        return  balanceDetailsMapper(payableBalanceTransList);
    }

    private List<BalanceDetailsVO> getPayeeTranscationList(String cifNumber){
        List<Object> payeeBalanceTransList = balanceRelationShipsRepository.retrievePayeeBalanceListByCifList(cifNumber);
        return  balanceDetailsMapper(payeeBalanceTransList);
    }

    private List<BalanceDetailsVO>  balanceDetailsMapper(List<Object> oweToList){
        BalanceDetailsVO balanceDetailsVO=null;
        List<BalanceDetailsVO> payableTransList=new ArrayList<>();
        for(Object transaction:oweToList){
            try {
                Object[] obj=(Object[])transaction;
                String name=String.join(obj[1].toString(),obj[0].toString(),"");
                BigDecimal amount=new BigDecimal(obj[2].toString());
                balanceDetailsVO=BalanceDetailsVO.builder().name(name).amount(amount).build();
                payableTransList.add(balanceDetailsVO);
            }catch (Exception ex){
                logger.error("balanceDetailsMapper ::converting object into vo giving error");
            }
        }
        return payableTransList;
    }
}
