package com.wallet.implementation;

import com.wallet.entity.Balances;
import com.wallet.entity.Customer;
import com.wallet.repository.BalanceRepository;
import com.wallet.repository.CustomerRepository;
import com.wallet.service.CustomerService;
import com.wallet.service.PasswordEncryptionService;
import com.wallet.vo.LoginVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.security.auth.login.LoginException;
import java.math.BigDecimal;
import java.util.UUID;

@Service
public class CustomerServiceImpl implements CustomerService {

    private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private BalanceRepository balanceRepository;
    @Autowired
    private PasswordEncryptionService passwordEncryptionService;

    @Value("${assignment.login.failed.threshold.count}")
    private int loginThresholdCount;


    @Override
    public LoginVo getCustomerByCustId(String customerId) {
        Customer customer = customerRepository.getByCustomerId(customerId);
        return mapCustomerDetails(customer);
    }

    @Override
    public LoginVo saveCustomer(Customer customer) throws LoginException {
        try {
            customer.setPassword(passwordEncryptionService.encrypt(customer.getPassword()));
            customer.setCifNumber(UUID.randomUUID().toString());
            customer = customerRepository.save(customer);
            // to ensure balance table have record to avoid no client found issue during transfer.
            balanceRepository.save(Balances.builder().balance(new BigDecimal("0")).cifNumber(customer.getCifNumber()).build());
        } catch (Exception ex){
            throw new LoginException("customer register failed " + customer.getCustomerId());
        }
        return LoginVo.builder().status("success").build();
    }

    @Override
    public LoginVo doLogin(LoginVo loginVo) throws LoginException {
        Customer customer = customerRepository.doLogin(loginVo.getCustomerId(), passwordEncryptionService.encrypt(loginVo.getPassword()));
        if (!StringUtils.isEmpty(customer) && customer.getLoginFailedCount() >= loginThresholdCount) {
            incremantLoginFailedCount(loginVo.getCustomerId());
            throw new LoginException("user locked");
        } else if (StringUtils.isEmpty(customer)) {
            logger.error("login failed  attempt for the customer id", loginVo.getCustomerId());
            incremantLoginFailedCount(loginVo.getCustomerId());
            throw new LoginException("user id or password not found " + loginVo.getCustomerId());
        }
        return mapCustomerDetails(customer);
    }

    private LoginVo mapCustomerDetails(Customer customer) {

        if (!ObjectUtils.isEmpty(customer)) {

            return LoginVo.builder().customerId(customer.getCustomerId()).cifNumber(customer.getCifNumber()
            ).name(String.join(customer.getLastName(), customer.getFirstName(), " ")).status("success").build();
        }
        return LoginVo.builder().build();
    }

    public void incremantLoginFailedCount(String customerId) {
        Customer customer = customerRepository.getByCustomerId(customerId);
        if (!StringUtils.isEmpty(customer)) {
            customer.setLoginFailedCount(customer.getLoginFailedCount() + 1);
            customerRepository.save(customer);
        } else {
            logger.error("login attempt failed and customer id is not available");
        }
    }
}
