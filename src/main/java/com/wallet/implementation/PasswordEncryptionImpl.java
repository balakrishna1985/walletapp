package com.wallet.implementation;

import com.wallet.service.PasswordEncryptionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Base64;

@Service
@AllArgsConstructor
public class PasswordEncryptionImpl implements PasswordEncryptionService {

    private static Base64.Encoder encoder;
    private static Base64.Decoder decoder;

    @Override
    public String encrypt(String encString) {
        try {
            encoder = Base64.getEncoder();
            return encoder.encodeToString(encString.getBytes());
        } catch (Exception e) {
            System.out.println("Error while encrypting: " + e.toString());
        }
        return null;
    }

    @Override
    public String decrypt(String decString) {
        try {
            decoder = Base64.getDecoder();
            return new String(decoder.decode(decString));
        } catch (Exception e) {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }
}
