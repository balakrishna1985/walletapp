package com.wallet.implementation;

import com.wallet.entity.BalanceRelationships;
import com.wallet.entity.Balances;
import com.wallet.entity.Customer;
import com.wallet.repository.BalanceRelationShipsRepository;
import com.wallet.repository.CustomerRepository;
import com.wallet.repository.PaymentRepository;
import com.wallet.service.PaymentService;
import com.wallet.vo.PaymentVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {
    private static final Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private BalanceRelationShipsRepository balanceRelationShipsRepository;

    @Override
    @Transactional
    public PaymentVo topUp(PaymentVo paymentVo) throws Exception {
        logger.info("Start balance top up for customer");
        try {
            List<BalanceRelationships> oweToList = balanceRelationShipsRepository.retrieveDestinationOwes(paymentVo.getCifNumber());
            if (!CollectionUtils.isEmpty(oweToList)) {
                updateBalanceOweRelationship(oweToList, paymentVo);
            }
            if (paymentVo.getAmount().doubleValue() > 0) {
                Balances balances = paymentRepository.getBalancesByCif(paymentVo.getCifNumber());
                if (balances != null) {
                    balances.setBalance(balances.getBalance().add(paymentVo.getAmount()));
                    paymentRepository.save(balances);
                }
                paymentVo.setTotalBalance(balances.getBalance());
            }

            logger.info("End balance top  up for customer");
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new Exception(ex.getMessage());
        }
        if (paymentVo.getTotalBalance() == null)
            paymentVo.setTotalBalance(new BigDecimal("0.00"));
        return paymentVo;
    }

    @Override
    @Transactional
    public PaymentVo transfer(PaymentVo paymentVo) throws Exception {
        logger.info("Start transfer for customer");
        /** set transfer parameters **/
        paymentVOMapper(paymentVo);

        try {
            Balances existingCustBalance = paymentRepository.getBalancesByCif(paymentVo.getCifNumber());
            if (paymentVo.getAmount().compareTo(existingCustBalance.getBalance()) > 0) {
                updateBalanceDetails(paymentVo, paymentVo.getAmount().subtract(existingCustBalance.getBalance()));
                if (existingCustBalance.getBalance().doubleValue() > 0) {
                    paymentVo.setAmount(existingCustBalance.getBalance());
                    existingCustBalance.setBalance(new BigDecimal("0.00"));
                    paymentRepository.save(existingCustBalance);
                    updateOweRecurringList(paymentVo);
                }

            } else {
                existingCustBalance.setBalance(existingCustBalance.getBalance().subtract(paymentVo.getAmount()));
                paymentRepository.save(existingCustBalance);
                updateOweRecurringList(paymentVo);
            }
            // this is show from ac total balance after transfer done
            paymentVo.setTotalBalance(existingCustBalance.getBalance());

            logger.info("End transfer for customer");
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new Exception(ex.getMessage());
        }
        if (paymentVo.getTotalBalance() == null)
            paymentVo.setTotalBalance(new BigDecimal("0.00"));
        return paymentVo;
    }

    private void updateOweRecurringList(PaymentVo paymentVo) {

        List<BalanceRelationships> oweToList = balanceRelationShipsRepository.retrieveDestinationOwes(paymentVo.getToCifNumber());
        if (!CollectionUtils.isEmpty(oweToList)) {
            updateBalanceOweRelationship(oweToList, paymentVo);
        }
        if (paymentVo.getAmount().doubleValue() > 0) {
            Balances toBalances = paymentRepository.getBalancesByCif(paymentVo.getToCifNumber());
            toBalances.setBalance(toBalances.getBalance().add(paymentVo.getAmount()));
            paymentRepository.save(toBalances);
        }
    }

    private void updateBalanceOweRelationship(List<BalanceRelationships> oweToList, PaymentVo paymentVo) {
        List<BalanceRelationships> updateList = new ArrayList<>();
        BigDecimal value = null;
        for (BalanceRelationships br : oweToList) {
            if (paymentVo.getAmount().compareTo(br.getOweingBalance()) >= 0) {
                value = paymentVo.getAmount().subtract(br.getOweingBalance());
                // update owing to balance
                updateCustomerBalanceByCif(br.getDestinationCifNumber(), br.getOweingBalance());
                // set owe to 0
                br.setOweingBalance(new BigDecimal("0.00"));
                updateList.add(br);
                paymentVo.setAmount(value);
            } else {
                value = br.getOweingBalance().subtract(paymentVo.getAmount());
                // update owing to balance
                updateCustomerBalanceByCif(br.getDestinationCifNumber(), paymentVo.getAmount());
                // set owe to remaining amount
                br.setOweingBalance(value);
                updateList.add(br);
                paymentVo.setAmount(new BigDecimal("0.00"));
                break;
            }

        }
        if (!CollectionUtils.isEmpty(updateList)) {
            balanceRelationShipsRepository.saveAll(updateList);
        }


    }

    private void updateBalanceDetails(PaymentVo paymentVo, BigDecimal remainingAmount) {
        BalanceRelationships balanceRelationships = balanceRelationShipsRepository.retrieveOweBalanceByCif(paymentVo.getCifNumber(), paymentVo.getToCifNumber());
        if (balanceRelationships != null) {
            BigDecimal amt = balanceRelationships.getOweingBalance().add(remainingAmount);
            balanceRelationships.setOweingBalance(amt);
        } else {
            balanceRelationships = BalanceRelationships.builder().sourceCifNumber(paymentVo.getCifNumber()).destinationCifNumber(paymentVo.getToCifNumber())
                    .oweingBalance(remainingAmount).createdBy(paymentVo.getCifNumber()).updatedBy(paymentVo.getCifNumber())
                    .build();
        }
        balanceRelationShipsRepository.save(balanceRelationships);


    }

    private void updateCustomerBalanceByCif(String cif, BigDecimal topupAmount) {
        logger.info("start balance top for customer : updateCustomerBalanceByCif");
        Balances balances = paymentRepository.getBalancesByCif(cif);
        if (balances != null) {
            balances.setBalance(balances.getBalance().add(topupAmount));
            paymentRepository.save(balances);
        }
        logger.info("End balance top for customer : updateCustomerBalanceByCif");
    }
    @Override
    @Transactional
    public Customer getCustomerDetailsByCif(String customerId) {
        return customerRepository.getByCustomerId(customerId);
    }
    public void paymentVOMapper(PaymentVo paymentVo){

        Customer fromCustomer=getCustomerDetailsByCif(paymentVo.getFromCustomerId());
        Customer toCustomer=getCustomerDetailsByCif(paymentVo.getToCustomerId());
        paymentVo.setCifNumber(fromCustomer.getCifNumber());
        paymentVo.setToCifNumber(toCustomer.getCifNumber());
    }
}
