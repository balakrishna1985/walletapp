package com.wallet.exceptions;

public class PaymentException extends java.security.GeneralSecurityException {
    public PaymentException(String msg) {
        super(msg);
    }
}
