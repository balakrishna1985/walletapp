package com.wallet.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "t_balance_relationships")
@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class BalanceRelationships {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long id;

    @Column(name = "source_cif_number", nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String sourceCifNumber;

    @Column(name = "destination_cif_number", nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String destinationCifNumber;

    @Column(name = "oweing_balance", nullable = false,scale = 2)
    private BigDecimal oweingBalance;

    @CreationTimestamp
    @Column(name = "created_date", nullable = false)
    private LocalDateTime createdDate;

    @UpdateTimestamp
    @Column(name = "updated_date", nullable = false)
    private LocalDateTime updatedDate;

    @Column(name = "updated_by", nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String updatedBy;

    @Column(name = "created_by", nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String createdBy;

}
