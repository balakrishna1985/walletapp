package com.wallet.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="t_transaction_log")
public class TransactionLog {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name="source_login_id",nullable = false)
    private String loginId;

    @Column(name="source_cif_number",nullable = false)
    private String sourceCifNumber;

    @Column(name="destination_cif_number",nullable = false)
    private String destinationCifNumber;

    @Column(name="transfer_amount",nullable = false)
    private double transferAmount;

    @CreationTimestamp
    @Column(name="created_date" ,nullable = false)
    private LocalDateTime createdDate;

    @UpdateTimestamp
    @Column(name="updated_date" ,nullable = false)
    private LocalDateTime updatedDate;

    @Column(name="updated_by" ,nullable = false)
    private String updatedBy;

    @Column(name="created_by" ,nullable = false)
    private String createdBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getSourceCifNumber() {
        return sourceCifNumber;
    }

    public void setSourceCifNumber(String sourceCifNumber) {
        this.sourceCifNumber = sourceCifNumber;
    }

    public String getDestinationCifNumber() {
        return destinationCifNumber;
    }

    public void setDestinationCifNumber(String destinationCifNumber) {
        this.destinationCifNumber = destinationCifNumber;
    }

    public double getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(double transferAmount) {
        this.transferAmount = transferAmount;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
