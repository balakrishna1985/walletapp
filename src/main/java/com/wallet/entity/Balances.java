package com.wallet.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_balances")
@Builder
@Data
public class Balances {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "cif_number", unique = true, nullable = false)
    private String cifNumber;

    @Column(name = "balance", nullable = false)
    private BigDecimal balance;

}
