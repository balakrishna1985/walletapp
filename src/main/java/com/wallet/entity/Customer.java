package com.wallet.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "t_customer")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "customer_id", unique = true, nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String customerId;

    @Column(name = "password", nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String password;

    @Column(name = "cif_number", unique = true, nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String cifNumber;

    @Column(name = "first_name", nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String firstName;

    @Column(name = "last_name")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String lastName;

    @Column(name = "email_id", nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String emailId;

    @Column(name = "updated_by", nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String updatedBy;

    @Column(name = "created_by", nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String createdBy;

    @CreationTimestamp
    @Column(name = "created_date", nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime createdDate;

    @UpdateTimestamp
    @Column(name = "updated_date", nullable = false)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime updatedDate;

    @UpdateTimestamp
    @Column(name = "last_login_date", nullable = false)
    private LocalDateTime lastLoginDate;

    @Column(name = "login_failed_count")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int loginFailedCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCifNumber() {
        return cifNumber;
    }

    public void setCifNumber(String cifNumber) {
        this.cifNumber = cifNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public LocalDateTime getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(LocalDateTime lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public int getLoginFailedCount() {
        return loginFailedCount;
    }

    public void setLoginFailedCount(int loginFailedCount) {
        this.loginFailedCount = loginFailedCount;
    }
}
