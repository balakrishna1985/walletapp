## Build:
   
   ## prerequigies
   
     1. install JDK1.8 and maven softwear in your sysystem
     2. clone the project from bit bucket.
	
   ## once clone the project run below command in the root folder
  
     1. mvn clean install

---

## Run the wallet application in local environment:

     1.navigate the target folder from the root folder
     2.run the below command for the running the wallet application
     3.java -jar wallet-1.0-SNAPSHOT.jar
   
---

## checking Swagger and H2 Database urls in browser.

    1 swagger url :

           http://localhost:8089/swagger-ui.html#/customer-controller

    2 H2 Database url and credentials.

        http://localhost:8089/h2/login.jsp

       Credentials for H2 Database
       
        username : admin
        password : admin

