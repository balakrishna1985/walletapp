import React, {useState} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import ShowModal from "./ShowModal";


class TransferPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            payOrTransfer: {
                toCustomerId: '',
                amount: '',
                comments:'',
            },
            submitted: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.confirmFundTransfer = this.confirmFundTransfer.bind(this);
        this.handlePayTransfer = this.handlePayTransfer.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { payOrTransfer } = this.state;
        this.setState({
            payOrTransfer: {
                ...payOrTransfer,
                [name]: value
            }
        });
    }

    handlePayTransfer(event) {
        event.preventDefault();
        const { payOrTransfer } = this.state;
        this.setState({ submitted: true });
        if(payOrTransfer.toCustomerId && payOrTransfer.amount) {
            this.setState({ openConfirm: true });
        }
    }

    confirmFundTransfer() {
        const { payOrTransfer } = this.state;
        const { user } = this.props;
        payOrTransfer.fromCustomerId = user.customerId;
        this.props.submitPayOrTransfer(payOrTransfer);
    }

    onCancel = () => {
        this.setState(() => ({ openConfirm:false}))
    }

    render() {
        const { payOrTransfer, submitted } = this.state;

        return (
            <>
                <nav className="navbar navbar-inverse navbar-fixed-top">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <a className="navbar-brand" href="#">Bank Application</a>
                        </div>
                        <div id="navbar" className="navbar-collapse collapse">
                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                    <Link to="/login">
                                        <button type="button" className="btn btn-success btn-lg">Logout</button>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="container-fluid" >
                   <br/>
                   <div className="col-md-4">
                   </div>
                   <div className="col-md-4">
                       <h2 className="page-header">Pay or Transfer</h2>
                       <form name="form"  data-target="#myModal" data-toggle="modal" onSubmit={this.handlePayTransfer}>
                           <div className={'form-group' + (submitted && !payOrTransfer.toCustomerId ? ' has-error' : '')}>
                                <label htmlFor="nameOrAccountNum">Name/Account Number</label>
                                <input type="text"
                                       className="form-control"
                                       name="toCustomerId"
                                       value={payOrTransfer.toCustomerId}
                                       onChange={this.handleChange} />
                                {submitted && !payOrTransfer.toCustomerId &&
                                    <div className="help-block">Name or Account Number required</div>
                                }
                           </div>
                           <div className={'form-group' + (submitted && !payOrTransfer.amount ? ' has-error' : '')}>
                                <label htmlFor="amount">Amount</label>
                                <input type="number"
                                       className="form-control"
                                       name="amount"
                                       value={payOrTransfer.amount}
                                       onChange={this.handleChange} />
                                    {submitted && !payOrTransfer.amount &&
                                        <div className="help-block">Amount is required</div>
                                    }
                           </div>
                           <div className={'form-group' + (submitted && !payOrTransfer.comments ? ' has-error' : '')}>
                                    <label htmlFor="comments">Comments</label>
                                    <input type="text"
                                           className="form-control"
                                           name="comments"
                                           value={payOrTransfer.comments}
                                           onChange={this.handleChange} />
                           </div>
                           <div className="form-group">
                               <button className="btn btn-primary">Submit</button>
                               <Link to="/" className="btn btn-link">Cancel</Link>
                           </div>
                       </form>
                   </div>
                </div>

                {this.state.openConfirm && <div className="" id="myModal">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content verify-modal-dialog">
                            <div>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={this.onCancel}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="container-fluid" style={{paddingTop: "20px", paddingBottom: "20px"}}>
                                <ul className="list-group">
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-6"><b>To CustomerID</b></div>
                                            <div className="col-md-6">{this.state.payOrTransfer.toCustomerId}</div>
                                        </div>
                                    </li>
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-6"><b>Amount</b></div>
                                            <div className="col-md-6">{this.state.payOrTransfer.amount}</div>
                                        </div>
                                    </li>
                                    <li className="list-group-item">
                                        <div className="row">
                                            <div className="col-md-6"><b>Comments</b></div>
                                            <div className="col-md-6">{this.state.payOrTransfer.comments}</div>
                                        </div>
                                    </li>
                                </ul>
                                <div className="form-group">
                                    <button className="btn btn-primary btn-block" onClick={this.confirmFundTransfer}>
                                        Confirm
                                    </button>
                                    <Link to="/" className="btn btn-light btn-block">Cancel</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                }

            </>
        );
    }
}

function mapState(state) {
    const { registering } = state.registration;
    const { cifData } = state.tocif;
    const { authentication } = state;
    const { user } = authentication;
    return { user, cifData, registering };
}

const actionCreators = {
    getToCifNumber: userActions.getToCifNumber,
    submitPayOrTransfer:userActions.submitPayOrTransfer
}

const connectedTransferPage = connect(mapState, actionCreators)(TransferPage);
export { connectedTransferPage as TransferPage };