import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';

class TopupPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            topup: {
                cifNumber: '',
                amount: '',
                comments:''
            },
            submitted: false
        };
        //localStorage.removeItem('user')
        this.handleChange = this.handleChange.bind(this);
        this.submitTopUp = this.submitTopUp.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { topup } = this.state;
        this.setState({
            topup: {
                ...topup,
                [name]: value
            }
        });
    }

    submitTopUp(event) {
        event.preventDefault();
        this.setState({ submitted: true });

        const { topup } = this.state;
        const { user } = this.props;
        topup.cifNumber = user.cifNumber;
        if (topup.amount && topup.cifNumber) {
           this.props.topupAmount(topup)
        }
    }

    render() {
        const { user  } = this.props;
        const { topup, submitted } = this.state;
        return (
            <>
                <nav className="navbar navbar-inverse navbar-fixed-top">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <a className="navbar-brand" href="#">Bank Application</a>
                        </div>
                        <div id="navbar" className="navbar-collapse collapse">
                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                    <Link to="/login">
                                        <button type="button" className="btn btn-success btn-lg">Logout</button>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="container-fluid" >
                    <br/>
                    <div className="col-md-4">
                    </div>
                    <div className="col-md-4">
                        <h2 className="page-header">TOP UP</h2>
                        <form name="form" onSubmit={this.submitTopUp}>
                            <div className={'form-group' + (submitted && !topup.amount ? ' has-error' : '')}>
                                <label htmlFor="amount">Amount</label>
                                <input type="number"
                                       className="form-control"
                                       name="amount"
                                       value={topup.amount}
                                       onChange={this.handleChange} />
                                {submitted && !topup.amount &&
                                <div className="help-block">Amount is required</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !topup.comments ? ' has-error' : '')}>
                                <label htmlFor="comments">Comments</label>
                                <input type="text"
                                       className="form-control"
                                       name="comments"
                                       value={topup.comments}
                                       onChange={this.handleChange} />
                            </div>
                            <div className="form-group">
                                <button className="btn btn-primary">Submit</button>
                                <Link to="/" className="btn btn-link">Cancel</Link>
                            </div>
                        </form>
                    </div>
                    <div className="col-md-4">
                    </div>
                </div>
            </>
        );
    }
}

function mapState(state) {
    const { registering } = state.registration;
    const { authentication } = state;
    const { user } = authentication;
    return { user, registering };
}

const actionCreators = {
    topupAmount: userActions.topupAmount
}

const connectedTopupPage = connect(mapState, actionCreators)(TopupPage);
export { connectedTopupPage as TopupPage };