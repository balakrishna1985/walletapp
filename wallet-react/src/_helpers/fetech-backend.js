// array in local storage for registered users
export function configureFakeBackend() {
    let realFetch = window.fetch;
    window.fetch = function (url, opts) {
        return new Promise((resolve, reject) => {
            // wrap in timeout to simulate server api call
            setTimeout(() => {
                // Register
                if (url.endsWith('/api/customer/register') && opts.method === 'POST') {
                    // get parameters from post request
                    let params = JSON.parse(opts.body);

                    const requestOptions = {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json; charset=UTF-8'
                        },
                        body: JSON.stringify(params)
                    };

                    realFetch(url, requestOptions)
                        .then(response => response.json())
                        .then(userDetails => {
                            if(userDetails && userDetails.status === 'success') {
                                    let responseJson = {
                                        customerId: params.customerId,
                                        username: params.emailId,
                                        firstName: params.firstName,
                                        lastName: params.lastName,
                                        token: 'fake-jwt-token'
                                    };
                                resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(responseJson)) });
                            } else {
                                reject('Username or password is incorrect');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        });
                    return;
                }

                // Authenticate
                if (url.endsWith('/api/doLogin') && opts.method === 'POST') {
                    // get parameters from post request
                    let params = JSON.parse(opts.body);

                    // find if any user matches login credentials
                    const requestOptions = {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json; charset=UTF-8'
                        },
                        body: JSON.stringify(params)
                    };

                    realFetch(url, requestOptions)
                        .then(response => response.json())
                        .then(loginDetails => {
                            if(loginDetails && loginDetails.status === 'success') {
                                let responseJson = {
                                    customerId: loginDetails.customerId,
                                    username: loginDetails.name,
                                    cifNumber: loginDetails.cifNumber,
                                    token: 'fake-jwt-token'
                                }
                                resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(responseJson)) })
                            } else {
                                reject('Username or password is incorrect');
                            }

                        })
                        .catch(error => {
                            console.log(error);
                        });

                    return;
                }

                // get account summary
                if (url.match(/\/api\/balance\/summary\/\d+$/) && opts.method === 'POST') {
                    const requestOptions = {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json; charset=UTF-8'
                        }
                    };

                    realFetch(url, requestOptions)
                        .then(response => response.json())
                        .then(summary => {
                            if(summary) {
                                let responseJson = {
                                    accountBalance: summary.accountBalance,
                                    payableBalanceTransList: summary.payableBalanceTransList,
                                    payeeBalanceTransList: summary.payeeBalanceTransList
                                }
                                // respond 200 OK with summary
                                resolve({ ok: true, text: () => JSON.stringify(responseJson)});
                            } else {
                                reject('No Summary Details');
                            }
                        })
                        .catch(error => {
                            console.log(error);
                        });

                    return;
                }

                // topup
                if (url.endsWith('/api/balance/topup') && opts.method === 'POST') {
                    // get parameters from post request
                    let params = JSON.parse(opts.body);

                    // send topup credentials
                    const requestOptions = {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json; charset=UTF-8'
                        },
                        body: JSON.stringify(params)
                    };

                    realFetch(url, requestOptions)
                        .then(response => response.json())
                        .then(topupDetails => {
                            //{
                            //     "amount": 234,
                            //     "cifNumber": "0e712077-30f1-4dfe-9367-9068babf22d7",
                            //     "totalBalance": 234,
                            //     "toCifNumber": null,
                            //     "customerName": null
                            // }

                            if(topupDetails && topupDetails.cifNumber) {
                                let responseJson = {
                                    amount: topupDetails.amount,
                                    cifNumber: topupDetails.cifNumber,
                                    totalBalance: topupDetails.totalBalance,
                                    token: 'fake-jwt-token'
                                }
                                resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(responseJson)) })
                            } else {
                                reject('Username or password is incorrect');
                            }

                        })
                        .catch(error => {
                            console.log(error);
                        });

                    return;
                }

                // fetch TO CIF Data
                if(url.match(/\/api\/customer\/get\/\d+$/) && opts.method === 'POST') {
                    // send to cif data
                    const requestOptions = {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json; charset=UTF-8'
                        }
                    };

                    realFetch(url, requestOptions)
                        .then(response => response.json())
                        .then(toCifData => {
                            console.log(toCifData);
                            // {
                            //     "customerId": "santhi.jaladi@gmail.com",
                            //     "password": null,
                            //     "cifNumber": "0e712077-30f1-4dfe-9367-9068babf22d7",
                            //     "name": "SanthiJaladi ",
                            //     "statusCode": null,
                            //     "status": "success"
                            // }

                        })
                        .catch(error => {
                            console.log(error);
                        });

                    return;
                }

                // get user by id
                if (url.match(/\/users\/\d+$/) && opts.method === 'GET') {
                    // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
                        // find user by id in users array
                        let urlParts = url.split('/');
                        let id = parseInt(urlParts[urlParts.length - 1]);
                        let matchedUsers = users.filter(user => { return user.id === id; });
                        let user = matchedUsers.length ? matchedUsers[0] : null;

                        // respond 200 OK with user
                        resolve({ ok: true, text: () => JSON.stringify(user)});
                    } else {
                        // return 401 not authorised if token is null or invalid
                        reject('Unauthorised');
                    }

                    return;
                }

                // register user
                if (url.endsWith('/users/register') && opts.method === 'POST') {
                    // get new user object from post body
                    let newUser = JSON.parse(opts.body);

                    // validation
                    let duplicateUser = users.filter(user => { return user.username === newUser.username; }).length;
                    if (duplicateUser) {
                        reject('Username "' + newUser.username + '" is already taken');
                        return;
                    }

                    // save new user
                    newUser.id = users.length ? Math.max(...users.map(user => user.id)) + 1 : 1;
                    users.push(newUser);
                    localStorage.setItem('users', JSON.stringify(users));

                    // respond 200 OK
                    resolve({ ok: true, text: () => Promise.resolve() });

                    return;
                }

                // delete user
                if (url.match(/\/users\/\d+$/) && opts.method === 'DELETE') {
                    // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
                        // find user by id in users array
                        let urlParts = url.split('/');
                        let id = parseInt(urlParts[urlParts.length - 1]);
                        for (let i = 0; i < users.length; i++) {
                            let user = users[i];
                            if (user.id === id) {
                                // delete user
                                users.splice(i, 1);
                                localStorage.setItem('users', JSON.stringify(users));
                                break;
                            }
                        }

                        // respond 200 OK
                        resolve({ ok: true, text: () => Promise.resolve() });
                    } else {
                        // return 401 not authorised if token is null or invalid
                        reject('Unauthorised');
                    }

                    return;
                }

                // pass through any requests not handled above
                realFetch(url, opts).then(response => resolve(response));

            }, 500);
        });
    }
}