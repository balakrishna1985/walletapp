import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
// import transactions from '../../mock/transactions.json';
import { userActions } from '../_actions';

class HomePage extends React.Component {

    componentDidMount() {
        const {user} = this.props;
        if (user.customerId && user.cifNumber) {
            const cifNumber = user.cifNumber;
            this.props.getUsers(cifNumber);
        }
    }

    render() {
        const { user, users } = this.props;
        const transactions = users?.summary?.payableBalanceTransList
        const receviedtransactions = users?.summary?.payeeBalanceTransList
        return (
            <>
                <nav className="navbar navbar-inverse navbar-fixed-top">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <a className="navbar-brand" href="#">Bank Application</a>
                        </div>
                        <div id="navbar" className="navbar-collapse collapse">
                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                   <Link to="/login">
                                       <button type="button" className="btn btn-success btn-lg">Logout</button>
                                   </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <div className="container-fluid">
                    <h2 className="page-header">Hello, <b>{user.username}</b></h2>
                    <div className="row">
                        <div className="col-sm-4  col-md-4">
                            <h3 className="sub-header text-left">Your available balance</h3>
                        </div>
                        <div className="col-sm-4  col-md-4">
                            <h3 className="sub-header text-left">SGD {users?.summary?.accountBalance}</h3>
                        </div>
                        <div className="col-sm-2  col-md-2">
                            <Link to="/topup" className="btn btn-warning btn-block">Top Up</Link>
                        </div>
                        <div className="col-sm-2  col-md-2 main">
                            <Link to="/transfer" className="btn btn-primary btn-block">Pay or Transfer</Link>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12 col-md-6" style={{background: "white"}}>
                            <h3 className="sub-header">You owe</h3>
                            <br/>
                            <div className="table-responsive">
                                <table className="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>NAME</th>
                                        <th>TRANSACTION AMOUNT</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {transactions &&
                                    transactions.map((owe, index) =>
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{owe.name}</td>
                                            <td>
                                                {'SGD '}
                                                {owe.amount}
                                            </td>
                                        </tr>
                                    )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="col-sm-12  col-md-6" style={{background: "white"}}>
                            <h3 className="sub-header">You get</h3>
                            <br/>
                            <div className="table-responsive">
                                <table className="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>NAME</th>
                                        <th>TRANSACTION AMOUNT</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {receviedtransactions &&
                                    receviedtransactions.map((youGet, index) =>
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{youGet.name}</td>
                                            <td>
                                                {'SGD '}
                                                {youGet.amount}
                                            </td>
                                        </tr>
                                    )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

function mapState(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return { user, users };
}

const actionCreators = {
    getUsers: userActions.getAccountSummary,
    deleteUser: userActions.delete
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export { connectedHomePage as HomePage };