import { userConstants } from '../_constants';

export function tocif(state = {}, action) {
    switch (action.type) {
        case userConstants.TO_CIF_REQUEST:
            return {
                type: 'to-cif-success',
                toCifData: action.toCifData
            };
        case userConstants.TO_CIF_SUCCESS:
            return {
                type: 'to-cif-success',
                toCifData: action.toCifData
            };
        case userConstants.TO_CIF_FAILURE:
            return {
            };
        default:
            return state
    }
}