import { userConstants } from '../_constants';
import { userService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const userActions = {
    login,
    logout,
    register,
    getAccountSummary,
    topupAmount,
    getToCifNumber,
    submitPayOrTransfer,
    delete: _delete
};

function login(customerId, password) {
    return dispatch => {
        dispatch(request({ customerId }));

        userService.login(customerId, password)
            .then(
                user => { 
                    dispatch(success(user));
                    history.push('/');
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
}

function logout() {
    userService.logout();
    return { type: userConstants.LOGOUT };
}

function register(user) {
    return dispatch => {
        dispatch(request(user));
        userService.register(user)
            .then(
                user => { 
                    dispatch(success());
                    history.push('/login');
                    dispatch(alertActions.success('Registration successful'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };
    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function topupAmount(topup) {
    return dispatch => {
        dispatch(request(topup));

        userService.topupAmount(topup)
            .then(
                topupAmt => {
                    dispatch(success());
                    history.push('/');
                    dispatch(alertActions.success('TopUp Submitted!'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(topup) { return { type: userConstants.TOPUP_REQUEST, topup } }
    function success(topup) { return { type: userConstants.TOPUP_SUCCESS, topup } }
    function failure(error) { return { type: userConstants.TOPUP_FAILURE, topup } }

}

function getToCifNumber(customerId) {
    return dispatch => {
        dispatch(request(customerId));

        userService.getToCifNumber(customerId)
            .then(
                toCifData => {
                    dispatch(success());
                    history.push('/transfer');
                    dispatch(alertActions.success('To Cif Fetch Successful!'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(cifData) { return { type: userConstants.TO_CIF_REQUEST, cifData } }
    function success(cifData) { return { type: userConstants.TO_CIF_SUCCESS, cifData } }
    function failure(error) { return { type: userConstants.TO_CIF_FAILURE, error } }
}

function submitPayOrTransfer(payOrTransfer) {
    return dispatch => {
        dispatch(request(payOrTransfer));

        userService.submitPayOrTransfer(payOrTransfer)
            .then(
                payOrTransfer => {
                    dispatch(success());
                    history.push('/');
                    dispatch(alertActions.success('Transfer Submitted!'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(payOrTransfer) { return { type: userConstants.PAYTRANSFER_REQUEST, payOrTransfer } }
    function success(payOrTransfer) { return { type: userConstants.PAYTRANSFER_SUCCESS, payOrTransfer } }
    function failure(error) { return { type: userConstants.PAYTRANSFER_FAILURE, error } }

}

function getAccountSummary(cifNumber) {
    return dispatch => {
        dispatch(request());

        userService.getAccountSummary(cifNumber)
            .then(
                summary => dispatch(success(summary)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: userConstants.GETACCSUMMARY_REQUEST } }
    function success(summary) { return { type: userConstants.GETACCSUMMARY_SUCCESS, summary } }
    function failure(error) { return { type: userConstants.GETACCSUMMARY_FAILURE, error } }
}

function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        userService.delete(id)
            .then(
                user => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
}